<?php
namespace FrontIt\Database;

/**
 * Simple database abstraction layer
 *
 * @package FrontIt\Database
 */
class Connection
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var QueryProfiler
     */
    private $queryProfiler;

    /**
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
    {
        $this->pdo = $connection;
    }

    /**
     * Setter for query profiler. It could be not set, then profiling wont work
     *
     * @param QueryProfiler $profiler
     */
    public function setQueryProfiler(QueryProfiler $profiler)
    {
        $this->queryProfiler = $profiler;
    }

    /**
     * Deletes from given table
     *
     * @param string $table
     * @param array  $criteria
     * @param array  $types
     *
     * @return int
     * @throws \Exception
     */
    public function delete($table, array $criteria, array $types = [])
    {
        if (empty($criteria)) {
            throw new \Exception('Empty criteria for deletion');
        }

        $deleteFields = [];

        foreach (array_keys($criteria) as $column) {
            $deleteFields[] = $column . ' = ?';
        }

        $sql = 'DELETE FROM ' . $table . ' WHERE ' . implode(' AND ', $deleteFields);

        return $this->executeQuery($sql, array_values($criteria), $types);
    }

    /**
     * Executes insert or update query
     *
     * @param string $query
     * @param array  $parameters
     * @param array  $types
     *
     * @return int
     */
    private function executeQuery($query, array $parameters = [], array $types = [])
    {
        if ($this->queryProfiler) {
            $this->queryProfiler->startQuery($query, $parameters, $types);
        }

        if (count($parameters)) {
            $statement = $this->pdo->prepare($query);
            if (count($types)) {
                $this->bindValues($statement, $parameters, $types, true);
                $statement->execute();
            } else {
                $statement->execute($parameters);
            }

            $result = $statement->rowCount();
        }

        if ($this->queryProfiler) {
            $this->queryProfiler->stopQuery();
        }

        return $result;
    }

    /**
     * Binds values for the statement
     *
     * @param \PDOStatement $statement
     * @param array         $parameters
     * @param array         $types
     * @param boolean       $numeric
     */
    private function bindValues($statement, array $parameters, array $types, $numeric = false)
    {
        $i = 1;
        foreach ($parameters as $key => $value) {
            if (isset($types[$key])) {
                $statement->bindValue($numeric ? $i : $key, $value, $types[$key]);
            } else {
                $statement->bindValue($numeric ? $i : $key, $value);
            }
            $i++;
        }
    }

    /**
     * Updates records in the database
     *
     * @param string $table
     * @param array  $data
     * @param array  $criteria
     * @param array  $types
     *
     * @return int
     */
    public function update($table, array $data, array $criteria, array $types = [])
    {
        $updateFields = [];

        foreach ($data as $key => $value) {
            $updateFields[] = $key . ' = ?';
        }

        $params = array_merge(array_values($data), array_values($criteria));
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $updateFields)
            . ' WHERE ' . implode(' = ? AND ', array_keys($criteria))
            . ' = ?';

        return $this->executeQuery($sql, $params, $types);
    }

    /**
     * Inserts entry to the database
     *
     * @param string $table
     * @param array  $data
     * @param array  $types
     *
     * @return int
     */
    public function insert($table, array $data, array $types = [])
    {
        if (empty($data)) {
            throw new \Exception('Data could not be empty');
        }

        return $this->executeQuery(
            "INSERT INTO {$table} (" . $this->getImplodedDataFields($data) . ") VALUES (" . $this->getParameterFillingsForData($data) . ")",
            array_values($data),
            $types
        );
    }

    /**
     * Implodes data fields into comma separated string
     *
     * @param array $data
     *
     * @return string
     */
    private function getImplodedDataFields(array $data)
    {
        return implode(', ', array_keys($data));
    }

    /**
     * Fills with dummy question marks for binding later
     *
     * @param array $data
     *
     * @return string
     */
    private function getParameterFillingsForData(array $data)
    {
        return implode(', ', array_fill(0, count($data), '?'));
    }

    /**
     * Gets associative array from the database
     *
     * @param string $query
     * @param array  $parameters
     * @param array  $types
     *
     * @return mixed
     */
    public function fetchAssoc($query, array $parameters = [], array $types = [])
    {
        return $this->executeSelectQuery($query, $parameters, $types)->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**
     * Executes select queries
     *
     * @param string $query
     * @param array  $parameters
     * @param array  $types
     *
     * @return \PDOStatement
     */
    private function executeSelectQuery($query, array $parameters = [], array $types = [])
    {
        if ($this->queryProfiler) {
            $this->queryProfiler->startQuery($query, $parameters, $types);
        }

        if (count($parameters)) {
            $statement = $this->pdo->prepare($query);
            if (count($types)) {
                $this->bindValues($statement, $parameters, $types);
                $statement->execute();
            } else {
                $statement->execute($parameters);
            }
        } else {
            $statement = $this->pdo->query($query);
        }

        if ($this->queryProfiler) {
            $this->queryProfiler->stopQuery();
        }

        return $statement;
    }

    /**
     * Fetches numeric array
     *
     * @param string $query
     * @param array  $parameters
     * @param array  $types
     *
     * @return mixed
     */
    public function fetchArray($query, array $parameters = [], array $types = [])
    {
        return $this->executeSelectQuery($query, $parameters, $types)->fetchAll(\PDO::FETCH_NUM);
    }

    /**
     * Fetches just one column from the database
     *
     * @param string $query
     * @param array  $parameters
     * @param array  $types
     * @param int    $column
     *
     * @return string
     */
    public function fetchColumn($query, array $parameters = [], array $types = [], $column = 0)
    {
        return $this->executeSelectQuery($query, $parameters, $types)->fetchAll(\PDO::FETCH_COLUMN, $column);
    }

    /**
     * Gets last inserted id
     *
     * @return string
     */
    public function getLastInsertId()
    {
        return $this->pdo->lastInsertId();
    }

}
