<?php

namespace FrontIt\Database;

/**
 * Class QueryProfiler
 *
 * @package FrontIt\Database
 */
class QueryProfiler
{
    /**
     * File where to put profiling data
     */
    const logFile = 'logs/queries.log';

    /**
     * @var array info about the current query
     */
    private $currentQuery;

    /**
     * Clears logfile
     */
    public function cleanLogFile()
    {
        file_put_contents(self::logFile, '');
    }

    /**
     * @param string $query
     * @param array  $parameters
     * @param array  $types
     */
    public function startQuery($query, $parameters = [], $types = [])
    {
        $this->currentQuery = [
            'query'      => $query,
            'parameters' => print_r($parameters, true),
            'types'      => print_r($types, true),
            'startTime'  => microtime(true),
        ];
    }

    /**
     * stops query and then logs info to the file
     */
    public function stopQuery()
    {
        $this->currentQuery['stopTime'] = microtime(true);
        $this->currentQuery['time'] = round(($this->currentQuery['stopTime'] - $this->currentQuery['startTime']) / 60,
            6);

        $data = $this->getDataForOutput();

        $this->logData($data);

        $this->currentQuery = null;

    }

    /**
     * Builds output for file
     *
     * @return string
     */
    protected function getDataForOutput()
    {
        $data = "------------------------------\n"
            . print_r($this->currentQuery, true) . "\n" .
            "------------------------------\n";

        return $data;
    }

    /**
     * Logs data to the file
     *
     * @param string $data
     */
    private function logData($data)
    {
        file_put_contents(self::logFile, $data, FILE_APPEND);
    }
}
