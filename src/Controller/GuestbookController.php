<?php

namespace FrontIt\Controller;

use FrontIt\Database\Connection;

/**
 * Simple controller to return data
 *
 * @package FrontIt\Controller
 */
class GuestbookController
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * GuestbookController constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Gets guestbook entries from the database
     *
     * @return string
     */
    public function getPostsAction()
    {
        $posts = $this->connection->fetchAssoc('SELECT * FROM guestbook ORDER BY id DESC');

        array_walk_recursive($posts, function (&$postData) {
            $postData = nl2br(htmlspecialchars($postData));
        });

        return json_encode($posts);
    }

    /**
     * Creates new post
     *
     * @param $data
     *
     * @return string
     * @throws \Exception
     */
    public function createNewPostAction($data)
    {
        if ($data['user'] && $data['content']) {
            $this->connection->insert('guestbook',
                [
                    'user'    => $data['user'],
                    'content' => $data['content'],
                    'created' => date('Y-m-d H:i:s'),
                ]);

            return json_encode(['success' => true, 'message' => 'Your post was successfully saved.']);
        }

        return json_encode(['success' => false, 'error' => 'Please fill all empty fields.']);

    }

    /**
     * Other ones handling
     *
     * @return string
     */
    public function unknownMethodAction()
    {
        return json_encode(['success' => false, 'error' => 'Unknown action requested.']);
    }


}
