<?php
namespace FrontIt\Database;

class ConnectionTest extends \PHPUnit_Extensions_Database_TestCase
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * ConnectionTest constructor.
     *
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->pdo = new \PDO('sqlite::memory:');
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->createInitialTables($this->pdo);
    }

    private function createInitialTables(\PDO $pdo)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS `guestbook` (
                id INTEGER PRIMARY KEY   AUTOINCREMENT,
                content VARCHAR(50) NOT NULL DEFAULT '',
                user VARCHAR(20) NOT NULL DEFAULT '',
                created VARCHAR(20) NOT NULL DEFAULT ''
            )
        ";
        $pdo->exec($query);
    }

    /**
     * Seeds database
     *
     * @return \PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getDataSet()
    {
        return $this->createFlatXMLDataSet(__DIR__ . '/files/guestbook-seed.xml');
    }

    /**
     * Tests if exception is thrown when deleting without any criteria
     *
     * @throws \Exception
     */
    public function testEmptyDeleteException()
    {
        $connection = new Connection($this->pdo);

        $this->setExpectedException('Exception', 'Empty criteria for deletion');
        $connection->delete('guestbook', []);
    }

    /**
     * Tests if entry was deleted
     *
     * @throws \Exception
     */
    public function testDeleteEntry()
    {
        $connection = new Connection($this->pdo);

        $this->assertEquals(2, $this->getConnection()->getRowCount('guestbook'));

        $connection->delete('guestbook', ['id' => 1]);

        $this->assertEquals(1, $this->getConnection()->getRowCount('guestbook'));
    }

    /**
     * @return \PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    public function getConnection()
    {
        return $this->createDefaultDBConnection($this->pdo, ':memory:');
    }

    /**
     * Tests insert empty data set
     *
     * @throws \Exception
     */
    public function testEmptyInsertException()
    {
        $connection = new Connection($this->pdo);

        $this->setExpectedException('Exception', 'Data could not be empty');

        $connection->insert('guestbook', []);
    }

    /**
     * Tests insertion of new entry
     *
     * @throws \Exception
     */
    public function testNewEntryInsert()
    {
        $connection = new Connection($this->pdo);

        $connection->insert('guestbook', [
            'user'    => 'petriukas',
            'content' => 'Bla bla',
            'created' => '2015-04-26 12:14:20',

        ]);

        $dataset = $this->createFlatXMLDataSet(__DIR__ . '/files/guestbook-seed-after-insert.xml');
        $this->assertDataSetsEqual($dataset, $this->getConnection()->createDataSet());

        $this->assertEquals(3, $connection->getLastInsertId());

    }

    /**
     * Tests fetching one column
     */
    public function testFetchColumn()
    {
        $connection = new Connection($this->pdo);

        $result = $connection->fetchColumn('SELECT * FROM guestbook WHERE user = :user', [':user' => 'joe'], [], 1);

        $this->assertEquals('Hello buddy!', $result[0]);
    }

    /**
     * tests fetching simple array
     */
    public function testFetchArray()
    {
        $connection = new Connection($this->pdo);

        $result = $connection->fetchArray('SELECT * FROM guestbook WHERE user = :user', [':user' => 'joe']);

        $expected = [
            '1',
            'Hello buddy!',
            'joe',
            '2010-04-24 17:15:23',
        ];

        $this->assertEquals($expected, $result[0]);
    }

    /**
     * tests fetchin associative array
     */
    public function testFetchAssoc()
    {
        $connection = new Connection($this->pdo);

        $result = $connection->fetchAssoc('SELECT * FROM guestbook WHERE user = :user', [':user' => 'joe']);

        $expected = [
            'id'      => '1',
            'content' => 'Hello buddy!',
            'user'    => 'joe',
            'created' => '2010-04-24 17:15:23',
        ];

        $this->assertEquals($expected, $result[0]);
    }

    /**
     * tests update of existing entry
     */
    public function testUpdateEntry()
    {
        $connection = new Connection($this->pdo);

        $connection->update('guestbook', [
            'user'    => 'onute',
            'content' => 'Lietuva',
        ], ['id' => 1]);

        $dataset = $this->createFlatXMLDataSet(__DIR__ . '/files/guestbook-seed-after-update.xml');
        $this->assertDataSetsEqual($dataset, $this->getConnection()->createDataSet());
    }

    /**
     * Tests update with types defined
     */
    public function testUpdateWithTypes()
    {
        $connection = new Connection($this->pdo);

        $connection->update('guestbook',
            [
                'user'    => 'onute',
                'content' => 'Lietuva',
            ],
            ['id' => 1],
            [
                'user'    => \PDO::PARAM_STR,
                'content' => \PDO::PARAM_STR,
                'id'      => \PDO::PARAM_INT,
            ]
        );

    }

    /**
     * tests fetching associative array with defined types
     */
    public function testFetchAssocWithTypes()
    {
        $connection = new Connection($this->pdo);

        $result = $connection->fetchAssoc('SELECT * FROM guestbook WHERE id = :id', [
            'id' => 1,
        ], [
            'id' => \PDO::PARAM_INT,
        ]);

        $expected = [
            'id'      => '1',
            'content' => 'Hello buddy!',
            'user'    => 'joe',
            'created' => '2010-04-24 17:15:23',
        ];

        $this->assertEquals($expected, $result[0]);

    }

    /**
     * tests simple fetching assoc without any parameters defined
     */
    public function testSimpleFetchAssoc()
    {
        $connection = new Connection($this->pdo);

        $result = $connection->fetchAssoc('SELECT * FROM guestbook');

        $this->assertCount(2, $result);
    }

    /**
     * Tests simple select with profiler enabled to see if it breaks something
     */
    public function testSelectWithProfiler()
    {
        $connection = new Connection($this->pdo);
        $profiler = new QueryProfiler();
        $profiler->cleanLogFile();
        $connection->setQueryProfiler($profiler);

        $connection->fetchAssoc('SELECT * FROM guestbook');

        $file = QueryProfiler::logFile;
        $this->assertFileExists($file);
        $this->assertNotEmpty(file_get_contents($file));
    }

    /**
     * Tests simple insert with profiler enabled to see if it breaks something
     */
    public function testInsertWithProfiler()
    {
        $connection = new Connection($this->pdo);
        $profiler = new QueryProfiler();
        $profiler->cleanLogFile();
        $connection->setQueryProfiler($profiler);

        $connection->insert('guestbook', [
            'user'    => 'petriukas',
            'content' => 'Bla bla',
            'created' => '2015-04-26 12:14:20',

        ]);

        $file = QueryProfiler::logFile;
        $this->assertFileExists($file);
        $this->assertNotEmpty(file_get_contents($file));
    }

}
