<?php

require __DIR__ . "/vendor/autoload.php";

$pdo = new PDO('sqlite:guestbook.sqlite');
$connection = new \FrontIt\Database\Connection($pdo);
$connection->setQueryProfiler(new \FrontIt\Database\QueryProfiler());

$controller = new \FrontIt\Controller\GuestbookController($connection);
header('Content-Type: application/json');

if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_GET['action'] === 'newPost') {
    echo $controller->createNewPostAction($_REQUEST);
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && $_GET['action'] === 'getPosts') {
    echo $controller->getPostsAction();
} else {
    echo $controller->unknownMethodAction();
}
