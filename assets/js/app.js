var guestbookApp = angular.module('guestbookApp', ['ngSanitize']);

guestbookApp
    .service('postsService', function ($http) {
        this.getPosts = function () {
            return $http.get('app.php?action=getPosts')
        };

        this.addPost = function (postData) {
            return $http({
                method: 'POST',
                url: 'app.php?action=newPost',
                data: $.param(postData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        };
    }).filter('rawHtml', ['$sce', function($sce){
            return function(val) {
                return $sce.trustAsHtml(val);
            };
    }]).controller('formController', function ($scope, $rootScope, $timeout, $http, postsService) {
        $scope.formData = {};
        $scope.submitForm = function (isValid) {
            if (isValid) {
                postsService.addPost($scope.post).success(function (data) {
                    if (!data.success) {
                        $scope.errorForm = data.error;
                        $scope.message = '';
                    } else {
                        $scope.message = data.message;
                        $scope.errorForm = '';
                        $scope.postForm.$setPristine();
                        $scope.post = '';

                        postsService.getPosts().success(function (data) {
                            $timeout(function() {
                                $rootScope.posts = data;
                            }, 0);
                        }).error(function () {
                            $scope.errorForm = 'Error happened while getting new posts';
                            $scope.message = '';
                        });
                    }
                }).error(function () {
                    $scope.errorForm = 'Error happened while posting the form';
                    $scope.message = '';
                });
            } else {
                $scope.errorForm = 'Please fill all form fields!';
                $scope.message = '';
            }
        };
    }).controller('listController', function ($scope, $rootScope, postsService) {
        postsService.getPosts().success(function (data) {
            $rootScope.posts = data;
        }).error(function () {
            $scope.errorForm = 'Error happened while getting new posts';
            $scope.message = '';
        });
    });

