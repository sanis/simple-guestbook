## Installation

Just clone this project to your computer and start project by running commands below.

To start virtual machine run ```vagrant up```

Afterwards access it and run composer install by running these commands:

```
vagrant ssh
cd /var/www/public
composer install
```

After composer install project should be up and can be accessed using this url:
http://192.168.33.10/

## Running tests

To run tests you should have box running and run these commands:

```
vagrant ssh
cd /var/www/public
php /vendor/bin/phpunit
```

## File locations

* Javascript files are located at ```assets/js```
* PHP code is located at ```src/```
* PHP unit tests and fixtures are located at ```tests/```
* Queries profiler data is located at ```logs/queries.log```

## About the code

Virtual machine is taken from https://box.scotch.io/ 

For a example and testing SQLite database is used (tests uses memory storage, app itself uses ```guestbook.sqlite```).

To get virtual machine running please keep in mind that you will need vagrant running.
